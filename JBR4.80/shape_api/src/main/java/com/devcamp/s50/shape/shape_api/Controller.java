package com.devcamp.s50.shape.shape_api;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.s50.shape.shape_api.model.Circle;
import com.devcamp.s50.shape.shape_api.model.Rectangle;
import com.devcamp.s50.shape.shape_api.model.Square;

@RestController
public class Controller {
    @CrossOrigin
    //api circle-area
    @GetMapping("circle-area")
    public double getCircleArea(@RequestParam(value="radius") double radius) {
        Circle newCircle = new Circle(radius);
        return newCircle.getArea();
    }
    //api circle-perimeter
    @GetMapping("circle-perimeter")
    public double getCirclePerimeter(@RequestParam(value="radius") double radius) {
        Circle newCircle = new Circle(radius);
        return newCircle.getPerimeter();
    }
    //api rectangle-area
    @GetMapping("rectangle-area")
    public double getRectangleArea(@RequestParam(value="width") double width, @RequestParam(value="length") double length ) {
        Rectangle newRectangle = new Rectangle(10,20);
        return newRectangle.getArea();
    }
    //api rectangle-perimeter
    @GetMapping("rectangle-perimeter")
    public double getRectanglePerimeter(@RequestParam(value="width") double width, @RequestParam(value="length") double length ) {
        Rectangle newRectangle = new Rectangle(width,length);
        return newRectangle.getPerimeter();
    }
    //api square-area
    @GetMapping("square-area")
    public double getSquareArea(@RequestParam(value="side") double side) {
        Square newSquare = new Square(side);
        return newSquare.getArea();
    }
    //api square-perimeter
    @GetMapping("square-perimeter")
    public double getSquarePerimeter(@RequestParam(value="side") double side) {
        Square newSquare = new Square(side);
        return newSquare.getPerimeter();
    }
}
