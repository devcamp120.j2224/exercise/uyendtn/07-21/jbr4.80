package com.devcamp.s50.shape.shape_api.model;

public class Circle extends Shape{
    double radius = 1.0;

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    public Circle(String color, boolean filled, double radius) {
        super(color, filled);
        this.radius = radius;
    }

    public Circle(double radius) {
        this.radius = radius;
    }

    public Circle() {
    }

    public double getPerimeter() {
        return 2*(Math.PI)*radius;
    }

    public double getArea() {
        return (Math.PI)*radius*radius;
    }

    public String toString() {
        return "Circle[" + super.toString() + ", radius= " + radius +"]";
    }
}
