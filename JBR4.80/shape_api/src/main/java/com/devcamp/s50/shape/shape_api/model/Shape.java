package com.devcamp.s50.shape.shape_api.model;

public class Shape {
    String color = "red";
    boolean filled = true;

    public String getColor() {
        return color;
    }
    public void setColor(String color) {
        this.color = color;
    }
    
    public boolean isFilled() {
        return filled;
    }
    public void setFilled(boolean filled) {
        this.filled = filled;
    }

    public Shape(String color, boolean filled) {
        this.color = color;
        this.filled = filled;
    }
    public Shape() {
    }

    public String toString() {
        return "Shape[color = " + color + ", filled = " + filled + "]";
    }

}
