package com.devcamp.s50.shape.shape_api.model;

public class Square extends Rectangle {

    public Square(String color, boolean filled, double side) {
        super(color, filled, side, side);
    }

    public Square(double side) {
        super(side, side);
    }

    public Square() {
    }
    
    public double getSide() {
        return super.length;
    }

    public void setSide(double side) {
        super.length = side;
    }
    
    public void setWidth(double side) {
        this.width = side;
    }

    public void setLength(double side) {
        this.length = side;
    }

    public String toString() {
        return "Square[" + super.toString() + "]";
    }
}
